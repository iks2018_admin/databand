package org.databandtech.api.service.impl;

import java.util.List;

import org.databandtech.api.entity.DatabandSite;
import org.databandtech.api.mapper.DatabandSiteMapper;
import org.databandtech.api.service.IDatabandSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * 站点Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandSiteServiceImpl implements IDatabandSiteService 
{
    @Autowired
    private DatabandSiteMapper databandSiteMapper;

    /**
     * 查询站点
     * 
     * @param id 站点ID
     * @return 站点
     */
    @Override
    public DatabandSite selectDatabandSiteById(Long id)
    {
        return databandSiteMapper.selectDatabandSiteById(id);
    }

    /**
     * 查询站点列表
     * 
     * @param databandSite 站点
     * @return 站点
     */
    @Override
    public List<DatabandSite> selectDatabandSiteList(DatabandSite databandSite)
    {
        return databandSiteMapper.selectDatabandSiteList(databandSite);
    }

    @Override
    public List<DatabandSite> selectDatabandSiteListAll()
    {
        return databandSiteMapper.selectDatabandSiteListAll();
    }

    /**
     * 新增站点
     * 
     * @param databandSite 站点
     * @return 结果
     */
    @Override
    public int insertDatabandSite(DatabandSite databandSite)
    {
        return databandSiteMapper.insertDatabandSite(databandSite);
    }

    /**
     * 修改站点
     * 
     * @param databandSite 站点
     * @return 结果
     */
    @Override
    public int updateDatabandSite(DatabandSite databandSite)
    {
        return databandSiteMapper.updateDatabandSite(databandSite);
    }



    /**
     * 删除站点信息
     * 
     * @param id 站点ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSiteById(Long id)
    {
        return databandSiteMapper.deleteDatabandSiteById(id);
    }
}