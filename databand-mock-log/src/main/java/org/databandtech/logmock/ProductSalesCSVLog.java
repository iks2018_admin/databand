package org.databandtech.logmock;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.databandtech.common.Mock;
import org.databandtech.logmock.entity.ProductOrderSale;
import org.databandtech.logmock.utils.Csv;

/**
 * 	产品销售日志
 */
public class ProductSalesCSVLog {

	static int YEAR = 2020;
	static int[] MONTHS = {10,11,12};
	static int COUNTLOW = 100000;//每天最少的订单数
	static int COUNTHIGH = 200000;//每天最多的订单数，每天的订单数会在两者之间随机
	static String[] CATEGORYS = {"PC","电视"};
	//日志会随机选择不同的厂家型号，多次出现是为了增加权重
	//不管具体型号了，就把厂家当成型号
	static String[] PRODUCTS_TV_MODEL = {"康佳","康佳","康佳","创维","创维","海信","海信","TCL","小米","海尔","长虹"};
	static String[] PRODUCTS_PC_MODEL = {"联想","联想","DELL","DELL","惠普","华硕","苹果","小米"};
	//为简单起见，所有的商品均 5000元；
	static int PRICE = 5000;
	//假设每次销售都有100、200、300随机的打折
	static int[] DISCOUNT = {100,100,100,200,300};
	static int[] BUYCOUNT = {1,1,1,1,1,1,1,2,2,3};//大部分情况下买1台
	static String[] CITYS = {"北京","北京","北京","上海","上海","上海","广州","广州","深圳","深圳","重庆","杭州","武汉","南京","郑州","西安","成都","长沙"};
	static String FILE_PATH = "c:\\logs\\csv\\product\\";
	
	public static void main(String[] args) {
		
		for (int month : MONTHS) {
			for (int day = 1;day<31;day++) {
				makeLogByDay("PC",month,day);
				makeLogByDay("电视",month,day);				
			}
		}
	}

	private static void makeLogByDay(String categoryId, int month, int day) {
		
		List<ProductOrderSale> list = new ArrayList<ProductOrderSale>();
		int randomCount = Mock.getNum(COUNTLOW, COUNTHIGH);
		for(int i=0;i<randomCount;i++) {
			ProductOrderSale p = new ProductOrderSale();
			p.setUserId(Mock.getCnName());
			p.setProductId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
			p.setSaleDatetime(YEAR+"-"+month+"-"+day);
			p.setCategoryId(categoryId);
			
			if (categoryId =="PC") {
				int indexPC = Mock.getNum(0, PRODUCTS_PC_MODEL.length-1);
				p.setModelId(PRODUCTS_PC_MODEL[indexPC]);
			}
			if (categoryId =="电视") {
				int indexTV = Mock.getNum(0, PRODUCTS_TV_MODEL.length-1);
				p.setModelId(PRODUCTS_TV_MODEL[indexTV]);
			}
			p.setColor("随机");
			p.setCityCode(CITYS[Mock.getNum(0, CITYS.length-1)]);
			int buyCount = BUYCOUNT[Mock.getNum(0, BUYCOUNT.length-1)];
			p.setBuyCount(buyCount);
			int buyDiscount = DISCOUNT[Mock.getNum(0, DISCOUNT.length-1)];
			p.setBuyDiscount(buyDiscount);
			p.setBuyTotle(PRICE * buyCount - buyDiscount );
			p.setAddress(Mock.getRoad());
			list.add(p);
		}
		
		String path = FILE_PATH + categoryId +"-" + YEAR +"-" +"-"+month+"-"+day + ".csv";
		Csv.writeCSV(list, path);
	}

}
