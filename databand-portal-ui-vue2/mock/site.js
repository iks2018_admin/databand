const Mock = require('mockjs')

const List = []
const count = 2

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    'value|1': ['1','2'],
    comment: '@title(5, 10)',
    'title|1': ['广告分析站点', '产品销售分析站点']   
  }))
}

module.exports = [
  {
    url: '/sites/list',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: {
          total: count,
          items: List
        }
      }
    }
  },
  {
    url: '/sites/menu',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            { key: 'PC', pv: 1024 },
            { key: 'mobile', pv: 1024 },
            { key: 'ios', pv: 1024 },
            { key: 'android', pv: 1024 }
          ]
        }
      }
    }
  }

  
]

