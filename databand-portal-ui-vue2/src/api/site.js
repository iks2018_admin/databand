import request from '@/utils/request'

export function getAllSite() {
    return request({
      url: '/sites/list',
      method: 'get'
    })
  }