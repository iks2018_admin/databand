package com.ruoyi.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.web.mapper.DatabandSitehistroyMapper;
import com.ruoyi.web.domain.DatabandSitehistroy;
import com.ruoyi.web.service.IDatabandSitehistroyService;
import com.ruoyi.common.core.text.Convert;

/**
 * 站点历史Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandSitehistroyServiceImpl implements IDatabandSitehistroyService 
{
    @Autowired
    private DatabandSitehistroyMapper databandSitehistroyMapper;

    /**
     * 查询站点历史
     * 
     * @param id 站点历史ID
     * @return 站点历史
     */
    @Override
    public DatabandSitehistroy selectDatabandSitehistroyById(Long id)
    {
        return databandSitehistroyMapper.selectDatabandSitehistroyById(id);
    }

    /**
     * 查询站点历史列表
     * 
     * @param databandSitehistroy 站点历史
     * @return 站点历史
     */
    @Override
    public List<DatabandSitehistroy> selectDatabandSitehistroyList(DatabandSitehistroy databandSitehistroy)
    {
        return databandSitehistroyMapper.selectDatabandSitehistroyList(databandSitehistroy);
    }

    /**
     * 新增站点历史
     * 
     * @param databandSitehistroy 站点历史
     * @return 结果
     */
    @Override
    public int insertDatabandSitehistroy(DatabandSitehistroy databandSitehistroy)
    {
        return databandSitehistroyMapper.insertDatabandSitehistroy(databandSitehistroy);
    }

    /**
     * 修改站点历史
     * 
     * @param databandSitehistroy 站点历史
     * @return 结果
     */
    @Override
    public int updateDatabandSitehistroy(DatabandSitehistroy databandSitehistroy)
    {
        return databandSitehistroyMapper.updateDatabandSitehistroy(databandSitehistroy);
    }

    /**
     * 删除站点历史对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSitehistroyByIds(String ids)
    {
        return databandSitehistroyMapper.deleteDatabandSitehistroyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除站点历史信息
     * 
     * @param id 站点历史ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSitehistroyById(Long id)
    {
        return databandSitehistroyMapper.deleteDatabandSitehistroyById(id);
    }
}
