package com.ruoyi.web.service;

import java.util.List;
import com.ruoyi.web.domain.DatabandSitehistroy;

/**
 * 站点历史Service接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface IDatabandSitehistroyService 
{
    /**
     * 查询站点历史
     * 
     * @param id 站点历史ID
     * @return 站点历史
     */
    public DatabandSitehistroy selectDatabandSitehistroyById(Long id);

    /**
     * 查询站点历史列表
     * 
     * @param databandSitehistroy 站点历史
     * @return 站点历史集合
     */
    public List<DatabandSitehistroy> selectDatabandSitehistroyList(DatabandSitehistroy databandSitehistroy);

    /**
     * 新增站点历史
     * 
     * @param databandSitehistroy 站点历史
     * @return 结果
     */
    public int insertDatabandSitehistroy(DatabandSitehistroy databandSitehistroy);

    /**
     * 修改站点历史
     * 
     * @param databandSitehistroy 站点历史
     * @return 结果
     */
    public int updateDatabandSitehistroy(DatabandSitehistroy databandSitehistroy);

    /**
     * 批量删除站点历史
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandSitehistroyByIds(String ids);

    /**
     * 删除站点历史信息
     * 
     * @param id 站点历史ID
     * @return 结果
     */
    public int deleteDatabandSitehistroyById(Long id);
}
