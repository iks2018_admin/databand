package org.databandtech.flink.keyselector;

import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;

/**
 * 实际key就是元组的第一个值
 * @author Administrator
 *
 */
public class Tuple2Selector implements KeySelector<Tuple2<String, Integer>, String> {
	private static final long serialVersionUID = 4780234853172462378L;
 
	@Override
	public String getKey(Tuple2<String, Integer> in) throws Exception {
		return in.f0;
	}
}
